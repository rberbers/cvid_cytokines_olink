# CVID_cytokines_olink

This repository will contain the R scripts used for the data analysis of our manuscript on targeted proteomics in Common Variable Immunodeficiency.

Content

0: LOD half
1: data prep
2: rarefaction curves
3: general overview (top 10 most abundant bacteria)
4: alpha diversity
5: beta diversity
6: ANCOM.2
6.1: plotting ANCOM results
7: bacterial load
8: unique taxa (not included in final version of manuscript)
9: HRCT scan scores

We are always looking to further develop our data analysis strategies for bacterial community sequencing data.
Please don't hesitate to contact me if you have any questions or suggestions!